<header class="app-header">
	<div class="app-logo">
		<img src="img/logo-entel.svg" alt="Logo Entel" class="app-logo-img">
		<img src="img/logo-entel-r.svg" alt="Logo Entel" class="app-logo-img--is-mobile">
	</div>
	<div class="app-options">
		<span id="BurgerIcon" class="material-icons app-options-burger">menu</span>

		<div class="app-options-buttons">
			<button class="button button-icon">
				<span class="material-icons">notifications</span>
			</button>

			<button class="button button-icon m-l-20">
				<span class="material-icons">search</span>
			</button>
		</div>
	</div>
	<div class="app-profile" >
		<!-- Carga dinámica de datos de perfil -->
		<profile-main>
			<!-- <div class="profile-photo">
				<img src="img/user.png" alt="Foto perfil" class="profile-photo-img">
			</div>
			<span class="profile-text profile-text--is-white m-l-20">LUIS ENRIQUE</span> -->			
			<img class="profile-icon" src="img/icons/dropdown.svg" alt="Flecha">
		</profile-main>
	</div>
</header>