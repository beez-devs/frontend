<profile-detail>
	<!-- Carga dinámica de datos de perfil -->
	<profile-detail-content>
		<!-- <div class="profile-photo">
			<img alt="Foto perfil" class="profile-photo-img">
		</div>
		<span class="profile-text profile-text--is-blue m-t-20"></span> -->
	</profile-detail-content>
	<profile-detail-menu>
		<div class="profile-detail-menu-option">
			<i class="material-icons profile-detail-menu-option-icon">edit</i>
			<span class="profile-detail-menu-option-text">Editar perfil</span>
		</div>
		<div class="profile-detail-menu-option" modal-open>
			<i class="material-icons profile-detail-menu-option-icon">photo</i>
			<span class="profile-detail-menu-option-text">Cambiar foto</span>
		</div>
		<div class="profile-detail-menu-option">
			<i class="material-icons profile-detail-menu-option-icon">logout</i>
			<span class="profile-detail-menu-option-text">Cerrar sesión</span>
		</div>
	</profile-detail-menu>
</profile-detail>