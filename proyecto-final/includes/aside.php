<aside class="app-aside">
	<aside-title>MENÚ DE OPCIONES</aside-title>
	
	<menu id="Mustache--is-content" class="block block-tab">
		<loading><img class="loading-img" src="img/loading.gif" alt=""></loading>
	</menu>

	<script id="Mustache--is-template" type="x-tmpl-mustache">
		{{#menu}}  
			
			<menu-option data-id="{{id}}" data-name="{{nombre}}" data-submenu="{{submenu}}">
				<button class="button button-tab">
					<i class="material-icons button--is-icon">{{icono}}</i>
					<span class="button-tab-name">{{nombre}}</span>
					{{#submenu}}<i class="material-icons button--is-arrow">arrow_drop_down</i>{{/submenu}}
				</button>					
			</menu-option>
			{{#submenu}}
				<nav class="button-tab-content">
					<ul class="button-tab-content-ul">
						{{#lista}}
						<li menu-sub-option class="button-tab-content-li" data-id="{{id}}" data-name="{{nombre}}">
							<a href="javascript:void(0)" class="button-tab-content-a">{{nombre}}</a>
						</li>
						{{/lista}}
					</ul>
				</nav>
            {{/submenu}}
		{{/menu}}
	</script>

	<!-- <menu class="block block-tab">					
		<menu-option>
			<button class="button button-tab ">
				<i class="material-icons button--is-icon">home</i>
				<span class="button-tab-name">Inicio</span>
				<i class="material-icons button--is-arrow">arrow_drop_down</i>
			</button>
		</menu-option>
		

		<menu-option class="menu-option-active">
			<button class="button button-tab">
				<i class="material-icons button--is-icon">pie_chart</i>
				<span class="button-tab-name">Tabla</span>
				<i class="material-icons button--is-arrow">arrow_drop_down</i>				
			</button>
			<nav class="button-tab-content">
				<ul class="button-tab-content-ul">
					<li class="button-tab-content-li">
						<a href="javascript:void(0)" class="button-tab-content-a">Opción 1</a>
					</li>
					<li class="button-tab-content-li">
						<a href="javascript:void(0)" class="button-tab-content-a">Opción 2</a>
					</li>
					<li class="button-tab-content-li">
						<a href="javascript:void(0)" class="button-tab-content-a">Opción 3</a>
					</li>
					<li class="button-tab-content-li">
						<a href="javascript:void(0)" class="button-tab-content-a">Opción 4</a>
					</li>
					<li class="button-tab-content-li">
						<a href="javascript:void(0)" class="button-tab-content-a">Opción 5</a>
					</li>
				</ul>
			</nav>
		</menu-option>
		
		<menu-option>
			<button class="button button-tab">
				<i class="material-icons button--is-icon">calendar_today</i>
				<span class="button-tab-name">Agenda</span>
				<i class="material-icons button--is-arrow">arrow_drop_down</i>
			</button>
		</menu-option>
	</menu> -->
	<img class="aside-logo" src="img/logo-everis.png" alt="Logo Everis">
</aside>



