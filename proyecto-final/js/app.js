"use strict";
const Everis = (function($, undefined) {   
    let events, suscribeEvents, beforeCatchDom, components, catchDom, dom, afterCatchDom, config, fn, initialize, st;
    st = {
        url: {
            // Rutaa para cargar datos del perfil
            profile : 'http://demo0947085.mockable.io/profile',
            // menu : 'http://demo0947085.mockable.io/menu'
            // Ruta para cargar datos del menu
            menu : 'http://demo0947085.mockable.io/menu',
            // Ruta para cargar grilla de datos
            language: 'data/autocomplete.json',
            persons: 'data/personas.json',
            grid: 'data/countries.json'
        },
        components : {
            profile : 'profile-main',
            profileDetail : 'profile-detail-content'
        },
        menuContent: "#Mustache--is-content",
        menuTemplate : "#Mustache--is-template",
        menuOpcion : "menu-option",
        uploadPreview : "#uploadPreview"
    };
    dom = {};
    beforeCatchDom = () => {
        
        fn.loadMenu()

        fn.loadLanguage()
        fn.loadPersons()
        fn.loadGrid1()
        fn.loadChart()
        fn.pieChart()
        fn.doughnut()


    };
    catchDom = () => {
        dom.menuContent     = $(st.menuContent)
        dom.menuTemplate    = $(st.menuTemplate)
        dom.uploadPreview   = $(st.uploadPreview)
    };
    afterCatchDom = () => {
       
        $( "#datepicker" ).datepicker();
        $( "#tabs" ).tabs();

    };
    suscribeEvents = () => {
        //Permite  mostrar el menu
        $("#BurgerIcon").on("click", events.abrirMenuPrincipal)
        //Permite mostrar el menu  perfil
        $("profile-main").on("click", events.abrirMenuPerfil)

        // Eventos para el menu ( aside )
        dom.menuContent.on( "click", "menu-option", events.abrirMenuOpcion)
        dom.menuContent.on( "click", "[menu-sub-option]", events.abrirMenuSubOpcion)

        //Evetno que me permite hace un cambio de foto de perfil
        $('#upload').on("change" , events.changePhoto)
        $('[modal-close]').on("click" , events.modalClose)
        $('[modal-open]').on("click" , events.modalOpen)
        $('[modal-save]').on("click" , (event) => {
            events.saveFoto(event, "odata")
        })
    };
    events = {
        abrirMenuPrincipal : (event) => {
            $("body").toggleClass("site--is-mobile")
        },
        abrirMenuPerfil : (event) => {
            $("body").toggleClass("profile--is-active")
        },
        abrirMenuOpcion : (event) => {

            // Attr , data : Me ayuda a encontrar un atributo específico del elemento
            // Captura el id: $(event.target).closest(st.menuOpcion).attr("data-id")
            // Captura el nombre: $(event.target).closest(st.menuOpcion).attr("data-name")

            // Parent, parents, closest : Me ayudan a profundizar con el elemento seleccionado
            let id      = $(event.target).closest(st.menuOpcion).data('id');
            let name = $(event.target).closest(st.menuOpcion).data('name');
            let submenu = $(event.target).closest(st.menuOpcion).data('submenu');

            // Resetar los estados activos de cada botón del menu
            //console.log( $(event.target).closest("menu").find(st.menuOpcion))
            $(event.target).closest("menu").find(st.menuOpcion).removeClass("menu-option-active")  

            // Si esque tiene el parámetro "data-submenu"=true 
            if(submenu){
                // Comprobar el estado de la opción
                let hasActive = $(event.target).closest(st.menuOpcion).hasClass("menu-option-active")
                if(!hasActive){
                    //Agregar estado activo a la opción
                    $(event.target).closest(st.menuOpcion).addClass("menu-option-active")
                    //Delizo hacia abajo las SubOpciones
                    $(event.target).closest(st.menuOpcion).next().slideDown("slow")
                }

            }else{
                //Delizo hacia arriba las SubOpciones
                $(st.menuOpcion).next('.button-tab-content').slideUp("slow")
                // Remuevo clase de activo a las Opciones
                $('[menu-sub-option]').removeClass('button-tab-content--is-active')
                // Agrego al que hize click el estado Activo
                $(event.target).closest("menu-option").addClass('menu-option-active')
                // Envio parámetros para Cargar contenido de la opción
                fn.mostrarContenido(id, name)
            }
            
        },
        abrirMenuSubOpcion: (event) => {
            // Identifico los parámetros necesisarios para cargar el contenido
            let id      = $(event.target).parent("[menu-sub-option]").data('id');
            let name    = $(event.target).parent("[menu-sub-option]").data('name');

            // Remuevo clase activo para todas la SubOpciones
            $(event.target).closest(".button-tab-content-ul").find("[menu-sub-option]").removeClass("button-tab-content--is-active")   
            // Agregar la clase Active a la opción elegida
            $(event.target).parent("[menu-sub-option]").addClass('button-tab-content--is-active')
            // Envío parámetros para mostrar el contenido
            fn.mostrarContenido(id, name)
        },
        changePhoto : (event) => {
            let fileInput   = document.getElementById("upload")
            let nameFile    = document.getElementById("uploadName")
            let reader      = new FileReader();

            //Detecto el archivo adjuntado
            let uploadFile = fileInput.files[0]
            //Detecto el nombre del archivo
            let uploadFileName = uploadFile.name

            //Compruebo archivo adjunto
            if (uploadFile) {
                // Metodo para el periodo de adjuntar imagen
                reader.onloadend = function (e) {
                    // Actualiamos la ruta del preview, a la ruta Base64 de la imagen
                    dom.uploadPreview.attr("src" , reader.result)
                    // Actualiamos el texto del nombre del archivo
                    nameFile.textContent = uploadFileName
                    // Cambios la ruta de la foto en una variable global
                    window.fotoPerfil = reader.result
                }
                // Mètodo importante del FileReader para dar lectura a la foto en Base64
                reader.readAsDataURL(uploadFile);
                //Callback para ver otros evetnos del FileReader
                readerAllEvents(reader);
            }

            function eventsFileReader(event) {
                console.table(`${event.type}: ${event.loaded} bytes transferidos`)
            }

            function readerAllEvents(reader) {
                reader.addEventListener('loadstart', eventsFileReader);
                reader.addEventListener('load', eventsFileReader);
                reader.addEventListener('loadend', eventsFileReader);
                reader.addEventListener('progress', eventsFileReader);
                reader.addEventListener('error', eventsFileReader);
                reader.addEventListener('abort', eventsFileReader);
            }
        },
        modalClose : (event) => {
            $("my-modal").attr("modal-status", "false")
        },
        modalOpen : (event) => {
            $("my-modal").attr("modal-status", "true")
        },
        saveFoto : (event, oData) => {
            events.modalClose()            
            $("[profile-img]").attr("src", window.fotoPerfil)            
        }
    };
    config = {
        coreService: (url, params, method, beforesend) => {
            let type = (method != undefined) ? method : "POST"
            let ajax = $.ajax({
                type: type,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                url: url,
                data: $.isArray(params) ? params : [] ,
                timeout: 30000,
                cache: false,
                beforeSend: () => {                    
                    if ($.isFunction(beforesend) === true) {
                       beforesend()
                    }
                },
                error: (xhr, ajaxOptions, thrownError) => {                
                  console.log(xhr);
                }   
            });
            return ajax;
        },
        runService: (url, params, method, beforesend, success, complete, error) => {            
            let service = config.coreService(url, params, method, beforesend);          
           
            if ($.isFunction(success) === true) {
                service.done(success);
            }
            if ($.isFunction(complete) === true) {
                service.always(complete);
            }
            if($.isFunction(error) === true) {
                service.fail(error);
            }

            return service
        }
    };
    components = {
        profile : ({data}) => {           
            return `
                <div class="profile-photo" data-id="${data[0].id}">
                    <img profile-img src="${window.fotoPerfil}" alt ="${data[0].nombre}" >
                </div>
                <span class="profile-text profile-text--is-white m-l-20">${data[0].nombre}</span>
            `;
        },
        profileDetail: (foto, nombre) => {
           return `
                <div class="profile-photo">
                    <img profile-img alt="${nombre}"  src="${foto}">
                </div>
                <span class="profile-text profile-text--is-blue m-t-20">${nombre}</span>
            `;
        },
        autocomplete : function (oData, id){
            $(id).autocomplete({
                source: oData
            });
        },
        selectpicker :function (id) {
            $(id).selectpicker();            
        }
    };
    fn = {
        loadProfile :  () => {            

            let service = new XMLHttpRequest();
            service.open('POST', st.url.profile);
            service.send();

            service.onload = function() {
                if (service.status != 200) { 
                    //console.log(`Error ${service.status}: ${service.statusText}`);  
                } else { 
                    //console.log(`Listo, ${service.response.length} bytes`); 

                    //Foto Perfil para todo el Proyecto
                    window.fotoPerfil = JSON.parse(service.response)["data"][0].foto
                    window.nombrePerfil = JSON.parse(service.response)["data"][0].nombre

                    // Cargar datos en el Dom de la pàgina                    
                    let oProfile = components.profile(JSON.parse(service.response))
                    $(oProfile).prependTo($(st.components.profile))        

                    // Cargar datos con un método en el Dom de la pàgina [Aside derecho]
                    let oProfileDetail = components.profileDetail(window.fotoPerfil, window.nombrePerfil)
                    $(oProfileDetail).prependTo($(st.components.profileDetail))

                    // En el DOM , varibale uploadPreview
                    dom.uploadPreview.attr('src', window.fotoPerfil)

                    //Cargar datos con un funciones jQuery en el Dom de la pàgina [Aside derecho]
                    // let oName = JSON.parse(service.response).data[0].nombre
                    // let oPict = JSON.parse(service.response).data[0].foto
                    // $('#Profile').find("span").text(oName)
                    // $('#Profile').find("img").attr("src", oPict)

                }
            };

            service.onprogress = function(event) {
                if (event.lengthComputable) {
                    // console.log(`Recibidos ${event.loaded} de ${event.total} bytes`);
                } else {
                    // console.log(`Recibidos ${event.loaded} bytes`); 
                }
            };

            service.onerror = function() {
                console.log("Request failed");
            };
        },
        testGrid : () => {
            $.ajax({
                url: st.url.grid,
                type: 'get',
                data: {
                    strNumResolucionFiltro: "119"
                },
                beforeSend: function(){
                    console.log("loading...")
                },
                error: function(jqXHR, textStatus, errorThrown){
                }
            }).done(function(response) {    
                console.log(response)  
            }).fail(function() {
            }).always(function(){
            });
        },
        loadMenu : () => {
            // Función que se ejecuta antes de que llegue los datos del servicio
            let isLoading = () => {
                // $(st.menuContent).html('<loading><img class="loading-img" src="img/loading.gif" alt=""></loading>')
            }
            // Función que se ejecuta al Obtener datos del servicio
            let isSuccess = () => {
            }
            // Función que se ejecuta al comprobar que el servicio el correcto
            let isComplete = () => {
                // Función para cargar el perfil
                fn.loadProfile()
            }
            // Función que se ejecuta cuando hubo un problema en el servicio
            let isError = () => {
            }

            // Promise de Ajax para manipular la respuesta del servicio
            // config.runService = ruta del servicio, parametros, tipo de servicio, funciones *** )
            let data = config.runService(st.url.menu , [] , "POST", isLoading, isSuccess , isComplete , isError )
            data.then(function(response, statusText, xhrObj) {
               
                // Hago que dentro de este contenedor, el html quede limpio
                dom.menuContent.html("")

                // Comienzo a parsear la data dentro del Contendor
                let template = document.getElementById('Mustache--is-template').innerHTML;
                let rendered = Mustache.render(template, response );
                dom.menuContent.html(rendered);
            })
        },
        mostrarContenido: (id, name)  => {
            // Ocultar todas las Plantillas Existentes
            $('main').hide()
            // Filtrar plantilla según opción del parámetro recibido y mostrar
            $('main').filter('[data-template="'+ id +'"]').show()
        },
        loadLanguage : function () {
            let data = config.runService(st.url.language)
            data.then(function(response, statusText, xhrObj) {
               components.autocomplete(response, "#tags")
            })
        },
        loadPersons: function() {
            let data = config.runService(st.url.persons)
            data.then(function(response, statusText, xhrObj) {
                // Comienzo a parsear la data dentro del Contendor
                let template = document.getElementById('Select--is-template').innerHTML;
                let rendered = Mustache.render(template, response );
                $('#Select--is-content').html(rendered);

                components.selectpicker("#Select--is-content")
            })
        },

        loadGrid1 : function(){

            let data = config.runService(st.url.grid)
            data.then(function(response, statusText, xhrObj) {
                DevExpress.localization.locale("es");

                $("#gridContainer").dxDataGrid({
                    dataSource: response,
                    columnAutoWidth:false,
                    columnHidingEnabled:false,
                     
                    selection: {
                        mode: "none",
                        allowSelectAll: true
                    },
                    grouping: {
                        autoExpandAll: false,
                    },
                    export: {
                      enabled: true,
                      allowExportSelectedData: true
                    },
                    onExporting: function(e) {
                      var workbook = new ExcelJS.Workbook();
                      var worksheet = workbook.addWorksheet('Employees');
                      
                      DevExpress.excelExporter.exportDataGrid({
                        component: e.component,
                        worksheet: worksheet,
                        autoFilterEnabled: true
                      }).then(function() {
                        // https://github.com/exceljs/exceljs#writing-xlsx
                        workbook.xlsx.writeBuffer().then(function(buffer) {
                          saveAs(new Blob([buffer], { type: 'application/octet-stream' }), 'Employees.xlsx');
                        });
                      });
                      e.cancel = true;
                    },
                    allowColumnReordering: true,
                    allowColumnResizing: true,
                    hoverStateEnabled: true,
                    filterRow: {
                        visible: false,
                        showOperationChooser: false,
                        applyFilter: "auto"
                    },
                    headerFilter: {
                        visible: false
                    },
                    sorting: {
                        mode: "multiple"
                    },
                    searchPanel: {
                        visible: true,
                        width: 250,
                        highlightCaseSensitive: true
                    },
                    paging: {
                        enabled: true,
                        pageSize: 10,
                        pageIndex: 0
                    },
                    pager: {
                        visible: true,
                        showPageSizeSelector: true,
                        allowedPageSizes: [10, 30, 100],
                        showInfo: true,
                        showNavigationButtons: true
                    },
                    rowAlternationEnabled: true,
                    filterRow: {
                        visible: true
                    },
                    
                    groupPanel: {
                        visible: true
                    },
                    columns: [
                        {
                            dataField: "CompanyName",
                        },
                        {
                            dataField: "City",
                            caption: "Ciudad"
                        },
                        {
                            dataField: "State",
                            caption: "Estado"
                        },
                        {
                            dataField: "Phone",
                            caption: "Telefono"
                        },
                        {
                            dataField: "Fax",
                            caption: "Faz"
                        },
                        {
                            dataField: "Percent",
                            caption: "Porcentaje",
                            cellTemplate: function (container, options) {

                                let html = '<div class="status-bar">'+
                                                '<span class="status-bar-fill" style="width: '+ options.data.Percent +'%;"></span>'+
                                                '<i class="status-bar-text">'+ options.data.Percent +'%</i>'+
                                            '</div>';
                                $(html).appendTo(container).end();


                            }
                        }                        
                    ]
                });
                         
                  
            
            })
        },

        loadChart: function(){
            var complaintsData = [
                { complaint: "Cold pizza", count: 780 },
                { complaint: "Not enough cheese", count: 120 },
                { complaint: "Underbaked or Overbaked", count: 52 },
                { complaint: "Delayed delivery", count: 1123 },
                { complaint: "Damaged pizza", count: 321 },
                { complaint: "Incorrect billing", count: 89 },
                { complaint: "Wrong size delivered", count: 222 }
            ];
            var data = complaintsData.sort(function (a, b) {
                return b.count - a.count;
            }),
            totalCount = data.reduce(function (prevValue, item) {
                return prevValue + item.count;
            }, 0),
            cumulativeCount = 0,
            dataSource = data.map(function (item, index) {
                cumulativeCount += item.count;
                return {
                    complaint: item.complaint,
                    count: item.count,
                    cumulativePercentage: Math.round(cumulativeCount * 100 / totalCount)
                };
            });

            $("#chart").dxChart({
                palette: "Harmony Light",
                dataSource: dataSource,
                title: "Pizza Shop Complaints",
                argumentAxis: {
                    label: {
                        overlappingBehavior: "stagger"
                    }
                },
                tooltip: {
                    enabled: true,
                    shared: true,
                    customizeTooltip: function (info) {
                        return {
                            html: "<div><div class='tooltip-header'>" +
                            info.argumentText + "</div>" +
                            "<div class='tooltip-body'><div class='series-name'>" +
                            info.points[0].seriesName +
                            ": </div><div class='value-text'>" +
                            info.points[0].valueText +
                            "</div><div class='series-name'>" +
                            info.points[1].seriesName +
                            ": </div><div class='value-text'>" +
                            info.points[1].valueText +
                            "% </div></div></div>"
                        };
                    }
                },
                valueAxis: [{
                    name: "frequency",
                    position: "left",
                    tickInterval: 300
                }, {
                    name: "percentage",
                    position: "right",
                    showZero: true,
                    label: {
                        customizeText: function (info) {
                            return info.valueText + "%";
                        }
                    },
                    constantLines: [{
                        value: 80,
                        color: "#fc3535",
                        dashStyle: "dash",
                        width: 2,
                        label: { visible: false }
                    }],
                    tickInterval: 20,
                    valueMarginsEnabled: false
                }],
                commonSeriesSettings: {
                    argumentField: "complaint"
                },
                series: [{
                    type: "bar",
                    valueField: "count",
                    axis: "frequency",
                    name: "Complaint frequency",
                    color: "#fac29a"
                }, {
                    type: "spline",
                    valueField: "cumulativePercentage",
                    axis: "percentage",
                    name: "Cumulative percentage",
                    color: "#6b71c3"
                }],
                legend: {
                    verticalAlignment: "top",
                    horizontalAlignment: "center"
                }
            });
        },

        pieChart : function(){
            var dataSource = [{
                country: "Russia",
                area: 12
            }, {
                country: "Canada",
                area: 7
            }, {
                country: "USA",
                area: 7
            }, {
                country: "China",
                area: 7
            }, {
                country: "Brazil",
                area: 6
            }, {
                country: "Australia",
                area: 5
            }, {
                country: "India",
                area: 2
            }, {
                country: "Others",
                area: 55
            }];
            $("#pie").dxPieChart({
                size: {
                    width: 500
                },
                palette: "bright",
                dataSource: dataSource,
                series: [
                    {
                        argumentField: "country",
                        valueField: "area",
                        label: {
                            visible: true,
                            connector: {
                                visible: true,
                                width: 1
                            }
                        }
                    }
                ],
                title: "Area of Countries",
                "export": {
                    enabled: true
                },
                onPointClick: function (e) {
                    var point = e.target;
            
                    toggleVisibility(point);
                },
                onLegendClick: function (e) {
                    var arg = e.target;
            
                    toggleVisibility(this.getAllSeries()[0].getPointsByArg(arg)[0]);
                }
            });
            
            function toggleVisibility(item) {
                if(item.isVisible()) {
                    item.hide();
                } else { 
                    item.show();
                }
            }
        },
        doughnut: function(){
            let dataSource = [{
                region: "Asia",
                val: 4119626293
            }, {
                region: "Africa",
                val: 1012956064
            }, {
                region: "Northern America",
                val: 344124520
            }, {
                region: "Latin America and the Caribbean",
                val: 590946440
            }, {
                region: "Europe",
                val: 727082222
            }, {
                region: "Oceania",
                val: 35104756
            }];
            $("#doughnut").dxPieChart({
                type: "doughnut",
                palette: "Soft Pastel",
                dataSource: dataSource,
                title: "The Population of Continents and Regions",
                tooltip: {
                    enabled: true,
                    format: "millions",
                    customizeTooltip: function (arg) {
                        var percentText = Globalize.formatNumber(arg.percent, {  
                            style: "percent",
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2
                        });
            
                        return {
                            text: arg.valueText + " - " + percentText
                        };
                    }
                },
                legend: {
                    horizontalAlignment: "right",
                    verticalAlignment: "top",
                    margin: 0
                },
                "export": {
                    enabled: true
                },
                series: [{        
                    argumentField: "region",
                    label: {
                        visible: true,
                        format: "millions",
                        connector: {
                            visible: true
                        }
                    }
                }]
            });
        }
    };
    initialize = () => {
        beforeCatchDom();
        catchDom();
        afterCatchDom();
        suscribeEvents();        
    };
    return {
        init: initialize
    };
})(jQuery);
Everis.init();