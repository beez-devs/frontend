

"use strict";
const Everis = (function($, undefined) {   
    let events, suscribeEvents, beforeCatchDom, components, catchDom, dom, afterCatchDom, config, fn, initialize, st;
    st = {
        
    };
    dom = {};
    beforeCatchDom = () => {
        
    };
    catchDom = () => {

    };
    afterCatchDom = () => {
    };
    suscribeEvents = () => {
        
    };
    events = {
       
    };
    config = {
        
    };
    components = {
       
    };
    fn = {
        
       
        
    };
    initialize = () => {
        beforeCatchDom();
        catchDom();
        afterCatchDom();
        suscribeEvents();        
    };
    return {
        init: initialize
    };
})(jQuery);
Everis.init();