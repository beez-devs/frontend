window.iniciarGMap = function() {
    var styles = [{
        stylers: [{
            hue: "#148dbf"
        }, {
            "saturation": -10
        }, {
            "lightness": 10
        }]
    }, {
        featureType: "road",
        elementType: "geometry",
        stylers: [{
            lightness: 10
        }, {
            visibility: "simplified"
        }]
    }, {
        featureType: "road",
        elementType: "labels",
        stylers: [{
            visibility: "on"
        }]
    }];

    var styledMap = new google.maps.StyledMapType(styles, {
        name: "Ania"
    });

    // Edit Ubication
    var miUbicacion = new google.maps.LatLng(-12.1749000, -77.0006999);
    // End Edi

    var mapOptions = {
        zoom: 17,
        center: miUbicacion,
        scrollwheel: false,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
        }
    };
    var map = new google.maps.Map(document.getElementById('load-gmap'), mapOptions);

    var contentString =
        '<div id="gm--message" class="cleaner">' +
        '<a class="gm--capa" href="https://goo.gl/cyy5xP" target="_blank"><span><i class="ico1 material-icons ">insert_emoticon</i></span></a>' +
        '<div class="top cleaner"><div class="cleaner tac"><img src="img/empresa-moncal.jpg" alt=""></div></div></div>' +
        '</div>' +
        '</div>' +
        '<div class="cleaner tac"><strong>Lavandería Moncal</strong></div>'

    var infowindow = new google.maps.InfoWindow({
        content: contentString,
        Width: 300
    });
    var marker = new google.maps.Marker({
        position: miUbicacion,
        map: map,
        title: "Lavandería Moncal",
        icon: 'img/place.png',
        animation: google.maps.Animation.DROP
    });
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map, marker);
    });
    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style');
}
