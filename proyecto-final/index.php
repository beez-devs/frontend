<!DOCTYPE html>
<html lang="es">
<head>
	<?php include "includes/meta-tags.php" ?>
	<?php include "includes/styles.php" ?>
	<?php include "includes/googleapis.php" ?>
	
 	<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.1/css/font-awesome.min.css">
 	<link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css" />

	<link rel="stylesheet" href="https://cdn3.devexpress.com/jslib/20.1.3/css/dx.common.css">
   <link rel="stylesheet" href="https://cdn3.devexpress.com/jslib/20.1.3/css/dx.light.css">

	<title>PROYECT EVERIS - ENTEL</title>
</head>
<body>
	<main-page class="profile--is-active2">

		<?php include "includes/header.php" ?>
		<app-content>
			<?php include "includes/aside.php" ?>

			<main class="app-main app-loadpage">
				<iframe src="https://cdpn.io/Ilyatsuprun/fullpage/aboNOwx" frameborder="0"></iframe>

		
			</main>

			<main class="app-main" data-template="1" style="display: none;">
				<breadcrumbs>
					<breadcrumbs-icon class="material-icons">home</breadcrumbs-icon>
					<breadcrumbs-namepage>Inicio</breadcrumbs-namepage>
				</breadcrumbs>

				<app-title>
					<h1 class="app-title-h1">Inicio</h1>
				</app-title>	

				<section class="box-card">



					<div class="dx-viewport">
					    <div class="demo-container">
					        <div id="gridContainer"></div>
					    </div>
					</div>




					
				</section>
			</main>

			<main class="app-main" data-template="3" style="display: none;">
				<breadcrumbs>
					<breadcrumbs-icon class="material-icons">home</breadcrumbs-icon>
					<breadcrumbs-namepage>Actividad</breadcrumbs-namepage>
				</breadcrumbs>

				<app-title>
					<h1 class="app-title-h1">JQUERY UI</h1>
				</app-title>	

				<section class="box-card">


					<!--  <select id="ListaPersona" class="" data-show-subtext="true" data-live-search="true">
				        <option data-subtext="Rep California">Tom Foolery</option>
				        <option data-subtext="Sen California">Bill Gordon</option>
				        <option data-subtext="Sen Massacusetts">Elizabeth Warren</option>
				        <option data-subtext="Rep Alabama">Mario Flores</option>
				        <option data-subtext="Rep Alaska">Don Young</option>
				        <option data-subtext="Rep California" disabled="disabled">Marvin Martinez</option>
				      </select> -->


					<select id="Select--is-content" data-show-subtext="true" data-live-search="true">
				 	</select>
					
					<script id="Select--is-template" type="x-tmpl-mustache">
						{{#personas}}
				        	<option data-subtext="{{nombre}}">{{nombre}}</option>
						{{/personas}}					       
					</script>



					<div class="ui-widget">
						<label for="tags">Language: </label>
						<input id="tags" class="form-input">
					</div>

					<div id="datepicker"></div>



					<div id="tabs">
						  <ul>
						    <li><a href="#tabs-1">Opcion 1</a></li>
						    <li><a href="#tabs-2">oPCION 2</a></li>
						    <li><a href="#tabs-3">Aenean lacinia</a></li>
						  </ul>
						  <div id="tabs-1">
						    <p>Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.</p>
						  </div>
						  <div id="tabs-2">
						    <p>Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.</p>
						  </div>
						  <div id="tabs-3">
						    <p>Mauris eleifend est et turpis. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem. Vestibulum non ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sodales. Quisque eu urna vel enim commodo pellentesque. Praesent eu risus hendrerit ligula tempus pretium. Curabitur lorem enim, pretium nec, feugiat nec, luctus a, lacus.</p>
						    <p>Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac lacus. Nulla facilisi. Praesent viverra justo vitae neque. Praesent blandit adipiscing velit. Suspendisse potenti. Donec mattis, pede vel pharetra blandit, magna ligula faucibus eros, id euismod lacus dolor eget odio. Nam scelerisque. Donec non libero sed nulla mattis commodo. Ut sagittis. Donec nisi lectus, feugiat porttitor, tempor ac, tempor vitae, pede. Aenean vehicula velit eu tellus interdum rutrum. Maecenas commodo. Pellentesque nec elit. Fusce in lacus. Vivamus a libero vitae lectus hendrerit hendrerit.</p>
						  </div>
					</div>

				</section>
			</main>

			<main class="app-main" data-template="4" style="display: none;">
				<breadcrumbs>
					<breadcrumbs-icon class="material-icons">home</breadcrumbs-icon>
					<breadcrumbs-namepage>Empresa</breadcrumbs-namepage>
				</breadcrumbs>

				<app-title>
					<h1 class="app-title-h1">Empresa</h1>
				</app-title>

				<section class="box-card">
					<section class="grid-card">
						<everis-table>
							<table class="grid">
								<thead class="grid-thead">
									<tr class="grid-thead-tr">
										<th class="grid-thead-th grid-column--is-center grid-column--is-extrasmall">
											<input type="checkbox" class="form-checkbox form-checkbox--is-center form-checkbox--is-th">
										</th>
										<th class="grid-thead-th ">Columna 2</th>
										<th class="grid-thead-th ">Columna 3</th>
										<th class="grid-thead-th ">Columna 3</th>
										<th class="grid-thead-th ">Columna 3</th>
										<th class="grid-thead-th ">Columna 3</th>
										<th class="grid-thead-th ">Columna 3</th>

										<th class="grid-thead-th grid-column--is-center grid-column--is-small">Acción</th>
									</tr>
								</thead>
								<tbody class="grid-tbody">
									<tr class="grid-tbody-tr">
										<td class="grid-tbody-td grid-column--is-center">
											<input type="checkbox" class="form-checkbox form-checkbox--is-center ">
										</td>
										<td class="grid-tbody-td ">Dato 2</td>
										<td class="grid-tbody-td ">Dato 3</td>
										<td class="grid-tbody-td ">Dato 3</td>
										<td class="grid-tbody-td ">Dato 3</td>
										<td class="grid-tbody-td ">Dato 3</td>
										<td class="grid-tbody-td ">Dato 3</td>
										
										<td class="grid-tbody-td grid-column--is-center">

											<everis-tooltip>
												<i class="material-icons grid-icon grid-icon--is-more">more_horiz</i>
												<everis-tooltip-content>
													<i class="material-icons grid-icon">edit</i>
													<i class="material-icons grid-icon">delete</i> 
												</everis-tooltip-content>

											</everis-tooltip>
										</td>
									</tr>
									
									
								</tbody>
							</table>
						</everis-table>
						<everis-paginator>
							<everis-paginator-page>
								Página <strong>1</strong> de <strong>20</strong>
							</everis-paginator-page>

							<everis-paginator-buttons>
								<button class="button button-paginator button-paginator--is-active">1</button>
								<button class="button button-paginator">2</button>
								<button class="button button-paginator">3</button>
								<button class="button button-paginator">4</button>		
							</everis-paginator-buttons>
							
						</everis-paginator>
					</section>
				</section>
			</main>
			
			<main class="app-main" data-template="5" style="display: none;">
				<breadcrumbs>
					<breadcrumbs-icon class="material-icons">home</breadcrumbs-icon>
					<breadcrumbs-namepage>Agenda</breadcrumbs-namepage>
				</breadcrumbs>

				<app-title>
					<h1 class="app-title-h1">Agenda</h1>
				</app-title>	

				<section class="box-card">
					
				   	<div class="dx-viewport">
					    <div class="demo-container">
					        <div id="chart-demo">
					            <div id="chart"></div>
					        </div>
					    </div>
					</div>


					<div class="demo-container">
				        <div id="pie"></div>
				    </div>


				    <div class="demo-container">
				        <div id="doughnut"></div>
				    </div>



				</section>
			</main>
			
			<?php include "includes/profile-detail.php" ?>

		</app-content>

		<my-modal modal-type="crear" modal-status="false">
	        <modal-box>
	            <modal-close modal-close>
	            	<span class="material-icons color-white">close</span>
	            </modal-close>
	            <modal-title>Cambiar Foto</modal-title>
	            <modal-content>                   
	                <div class="modal-flex">
	                    <div class="form-content form-content--is-auto">
	                        <uploadfile class="uploadfile">

	                        	<img profile-img id="uploadPreview" src="invalid_link" class="upload-photo" onerror="this.src='img/img-loading.png'">

					            <input  type="file" 
					                    accept="image/*"
					                    name="upload[]"
					                    accept=".jpg, .jpeg, .png"
					                    id="upload" 
					                    class="inputfile inputfile-6">
					            <label for="upload">
					                <span id="uploadName"></span>
					                <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path></svg> Elegir Foto </strong>
					            </label>
					        </uploadfile>
	                          
	                    </div>

	                    <div class="form-buttons m-t-30 t-a-c">
	                        <button class="button button-secondary" 
	                                type="button"
	                                modal-close >
	                                Cancelar
	                        </button>

	                        <button class="button button-primary m-l-20" 
	                                type="button"
	                                modal-save>
	                                Guardar
	                        </button>
	                    </div>
	                </div>
	            </modal-content>
	        </modal-box>
	    </my-modal>

		


	</main-page>


	<script type="text/javascript" src="vendors/jquery/jquery.min.js"></script>
	<script src="vendors/mustache/mustache.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

	
	<script src="https://cdn3.devexpress.com/jslib/20.1.3/js/dx.all.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/exceljs/3.3.1/exceljs.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.8/FileSaver.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/globalize/1.3.0/globalize.min.js"></script>
    <script src="vendors/devextreme/dx.pe.min.js"></script>
	<script type="text/javascript" src="js/app.js"></script>
</body>
</html>